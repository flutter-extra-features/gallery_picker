import 'package:flutter/material.dart';
import 'package:gallery_picker/gallery_picker.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<MediaFile> _selectedFiles = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildUI(),
      floatingActionButton: _selectImageFromGalleryButton(),
    );
  }

  Widget _selectImageFromGalleryButton() {
    return FloatingActionButton(
      onPressed: () async {
        List<MediaFile> mediaFiles = await GalleryPicker.pickMedia(
              context: context,
              singleMedia: false,
            ) ??
            [];
        setState(() {
          _selectedFiles = mediaFiles;
        });
      },
      child: const Icon(
        Icons.image,
      ),
    );
  }

  Widget _buildUI() {
    return GridView.builder(
      scrollDirection: Axis.horizontal,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
      ),
      itemCount: _selectedFiles.length,
      itemBuilder: (context, index) {
        MediaFile _file = _selectedFiles[index];
        if (_file.isImage) {
          return PhotoProvider(media: _file);
        } else if (_file.isVideo) {
          return VideoProvider(media: _file);
        }
      },
    );
  }
}
